require "sinatra"
require "sinatra/activerecord"

configure :development do
  set :database, {adapter: "sqlite3", database: "./dev.sqlite3"}
end

class Page < ActiveRecord::Base
end

get "/" do
  erb :home
end

get "/:page" do
  redirect "/" unless Page.exists?(url: params[:page])
  @page = Page.find_by(url: params[:page]).content
  erb :page
end