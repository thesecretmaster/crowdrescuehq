class Init < ActiveRecord::Migration[5.1]
  def change
    create_table :pages do |i|
      i.text :url
      i.text :content
    end
  end
end
